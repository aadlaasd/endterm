genG:
	protoc calculator/calculatorpb/calculator.proto --go_out=plugins=grpc:.
clean:
	rm calculator/calculatorpb/*.go
runs:
	 go run calculator/calculator_server/server.go

runc:
	 go run calculator/calculator_server/client.go

